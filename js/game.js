/*
* JS Tetris by Tom Tarpey (c) 2018
* This code is to help people learn 
* but in general we will use a copyleft 
* licence to save messing
*/

//hook to the canvas div
const canvas = document.getElementById("game");

// pull a 2d context from the canvas
const ctx = canvas.getContext("2d");

// scale up the context to allow single pixel suqares for optimization
ctx.scale(20, 20);


// clear a completed line
function boardSweep() {
  let rowCount = 1;
  outer: for (let y = board.length - 1; y > 0; --y) {
    for (let x = 0; x < board[y].length; ++x) {
      if (board[y][x] === 0) {
        continue outer;
      }
    }

    const row = board.splice(y, 1)[0].fill(0);
    board.unshift(row);
    ++y;

    player.score += rowCount * 10;
    rowCount *= 2;
  }
}

// collision detection takes in a board and a player returns true is a collision happens
function collide(board, player) {
  const m = player.board;
  const o = player.pos;
  for (let y = 0; y < m.length; ++y) {
    for (let x = 0; x < m[y].length; ++x) {
      if (m[y][x] !== 0 && (board[y + o.y] && board[y + o.y][x + o.x]) !== 0) {
        return true;
      }
    }
  }
  return false;
}

// used to create the board / playfield
function createBoard(w, h) {
  const board = [];
  while (h--) {
    board.push(new Array(w).fill(0));
  }
  return board;
}

// instantiate a piece on the screen of a given type
function createPiece(type) {
  if (type === "I") {
    return [[0, 1, 0, 0], [0, 1, 0, 0], [0, 1, 0, 0], [0, 1, 0, 0]];
  } else if (type === "L") {
    return [[0, 2, 0], [0, 2, 0], [0, 2, 2]];
  } else if (type === "J") {
    return [[0, 3, 0], [0, 3, 0], [3, 3, 0]];
  } else if (type === "O") {
    return [[4, 4], [4, 4]];
  } else if (type === "Z") {
    return [[5, 5, 0], [0, 5, 5], [0, 0, 0]];
  } else if (type === "S") {
    return [[0, 6, 6], [6, 6, 0], [0, 0, 0]];
  } else if (type === "T") {
    return [[0, 7, 0], [7, 7, 7], [0, 0, 0]];
  }
}

// draw the board to the canvas
function drawBoard(board, offset) {
  board.forEach((row, y) => {
    row.forEach((value, x) => {
      if (value !== 0) {
        ctx.fillStyle = colours[value];
        ctx.fillRect(x + offset.x, y + offset.y, 1, 1);
      }
    });
  });
}

// abstracted the drawing in to its own function for better decomposition
function draw() {
  ctx.fillStyle = "#000";
  ctx.fillRect(0, 0, canvas.width, canvas.height);

  drawBoard(board, { x: 0, y: 0 });
  drawBoard(player.board, player.pos);
}

// when a piece is to rest it is added to the playfield
function merge(board, player) {
  player.board.forEach((row, y) => {
    row.forEach((value, x) => {
      if (value !== 0) {
        board[y + player.pos.y][x + player.pos.x] = value;
      }
    });
  });
}


// rotation using some simple matrix manipulation a bit like in OpenGL
function rotate(board, dir) {
  for (let y = 0; y < board.length; ++y) {
    for (let x = 0; x < y; ++x) {
      [board[x][y], board[y][x]] = [board[y][x], board[x][y]];
    }
  }

  if (dir > 0) {
    board.forEach(row => row.reverse());
  } else {
    board.reverse();
  }
}

// moves the piece in a downward direction once per second and checks for collision
function playerDrop() {
  player.pos.y++;
  if (collide(board, player)) {
    player.pos.y--;
    merge(board, player);
    playerReset();
    boardSweep();
    updateScore();
  }
  counter = 0;
}

// move player left or right and check collision
function playerMove(offset) {
  player.pos.x += offset;
  if (collide(board, player)) {
    player.pos.x -= offset;
  }
}

// choise the next piece and reset player position
function playerReset() {
  const pieces = "TJLOSZI";
  player.board = createPiece(pieces[(pieces.length * Math.random()) | 0]);
  player.pos.y = 0;
  player.pos.x =
    ((board[0].length / 2) | 0) - ((player.board[0].length / 2) | 0);
  if (collide(board, player)) {
    board.forEach(row => row.fill(0));
    player.score = 0;
    updateScore();
  }
}

// abstracts player rotation and does collision detection
function playerRotate(dir) {
  const pos = player.pos.x;
  let offset = 1;
  rotate(player.board, dir);
  while (collide(board, player)) {
    player.pos.x += offset;
    offset = -(offset + (offset > 0 ? 1 : -1));
    if (offset > player.board[0].length) {
      rotate(player.board, -dir);
      player.pos.x = pos;
      return;
    }
  }
}

// timing code
let counter = 0;
let interval = 1000;

let previousTime = 0;
function update(time = 0) {
  const dalta = time - previousTime;

  counter += dalta;
  if (counter > interval) {
    playerDrop();
  }

  previousTime = time;

  draw();
  requestAnimationFrame(update); //for looping : timed recursive calls to the update function
}

// rendering of the player score in the score div
function updateScore() {
  document.getElementById("score").innerText = player.score;
}


// keyboard handling code
document.addEventListener("keydown", event => {
  if (event.keyCode === 37) { // left
    playerMove(-1);
  } else if (event.keyCode === 39) { // right
    playerMove(1);
  } else if (event.keyCode === 40) { // down
    playerDrop();
  } else if (event.keyCode === 90) { // rotate left
    playerRotate(-1);
  } else if (event.keyCode === 88) { // rotate right
    playerRotate(1);
  }
});

const colours = [ null, "#fa113d", "#5a17ed", "#fed024", "#617", "#acce55", "#fa771e", "#102ea1"]; // an array of colours for the pieces

const board = createBoard(12, 20); // create a new board / playfield

// the player object
const player = {
  pos: { x: 0, y: 0 },
  board: null,
  score: 0
};

// startup code for on load
playerReset();
updateScore();
update();
